/*!
*@file     GAEAUniqueManager.cpp
*@brief    GAEA全局唯一性实现
*@author   yangpy
*@date     2016年3月4日
*@remarks
*@version 3.0
*Copyright (c) 1998-2013 Glodon Corporation
*/
#include "StdAfx.h"
#include <Windows.h>
#include <QApplication>
#include <QSystemSemaphore>
#include <QSharedMemory>
#include <QWindow>
#include <QMainWindow>
#include <QTimer>
#include <sddl.h>

#include "TasGAEAUniqueManager.h"
#include "GMCommon/GMPSharedMemUtils.h"
#include "GLDFileUtils.h"

#include "GMPControls/GMPMessageBox.h"
#include "GMCommon/GMPFileUtils.h"
#include "GMPCore/GMPGlobalTags.h"
#include "GMPProject/GMPAppInfo.h"
#include "GMPControls/GMPBeginnerGuideDataManager.h"
#include "GMPCore/GMPServiceIntf.h"
#include "GMPCore/GMPMainForm.h"

static const int wp_OPENFILE = 0;   //打开文件
static const int wp_ACTIVATE = 1;   //激活窗口
static const wchar_t *c_szAppParamShareName = L"GAEASharedMemory";
static QSharedMemory s_InstanceMem;
unsigned int g_uMsgId = 0;

static const char *c_sGMPUniqueManager = "GMPUniqueManager";
static const char *c_smainInfo = QT_TRANSLATE_NOOP("GMPUniqueManager", "提示");
static const char *c_sMultipleProcessHit = QT_TRANSLATE_NOOP("GMPUniqueManager", "软件已在该电脑其它账户打开，请先退出~");
static const char *c_strInfo = QT_TRANSLATE_NOOP("GMPUniqueManager", "提示");
static const char *c_sOpenFile = QT_TRANSLATE_NOOP("GMPUniqueManager", "打开");
static const char *c_sFileIsOpening = QT_TRANSLATE_NOOP("GMPUniqueManager", "当前正在打开工程，请稍后再试。");
static const char *c_sOpenWhenModel = QT_TRANSLATE_NOOP("GMPUniqueManager", "请先关闭当前窗体，再重新双击打开工程！");

typedef LRESULT(CALLBACK *GMPWinProc)(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam);
GMPWinProc oldWinProc;

/**
* 新窗口过程，能够处理指定实例显示在屏幕最前面消息.
* @param[in]    HWND hwnd
* @param[in]    UINT uMsg
* @param[in]    WPARAM wParam
* @param[in]    LPARAM lParam
* @return       LRESULT CALLBACK
* @date  2018年4月10日
* @author yangpy
*/
LRESULT CALLBACK newGMPWinProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	if (uMsg == g_uMsgId)
	{
		unsigned int nLen = (unsigned int)lParam;
		char *pszParam = nullptr;
		//memset(pszParam, 0, nLen);
		if (!getMapFileData(c_szAppParamShareName, &pszParam, nLen))
		{
			delete[] pszParam;
			return 0;
		}

		QString strParam = QString::fromStdString(pszParam);
		QStringList args = strParam.split(";");
		delete[] pszParam;

		QWidget *pMainForm = QWidget::find((WId)hwnd);
		if (nullptr == pMainForm)
			return 0;

		QString strFilePath = getProjectFilePath(args);
		if (wParam == wp_ACTIVATE) //激活窗口
		{
			if (pMainForm->isMinimized())
			{
				pMainForm->setWindowState(pMainForm->windowState() & ~Qt::WindowMinimized);
				QApplication::processEvents();
			}

			DWORD dwCurID = GetCurrentThreadId();
			DWORD dwForeID = GetWindowThreadProcessId(GetForegroundWindow(), NULL);
			AttachThreadInput(dwCurID, dwForeID, TRUE);
			SetWindowPos(hwnd, HWND_TOPMOST, 0, 0, 0, 0, SWP_NOSIZE | SWP_NOMOVE);
			QWindow *pWindow = QApplication::modalWindow();
			SetWindowPos(hwnd, HWND_NOTOPMOST, 0, 0, 0, 0, SWP_NOSIZE | SWP_NOMOVE);

			if (pWindow == nullptr)
				SetForegroundWindow(hwnd);
			else
				SetForegroundWindow((HWND)pWindow->winId());

			AttachThreadInput(dwCurID, dwForeID, FALSE);
		}
		else if (wParam == wp_OPENFILE)
		{
			if (args.count() >= 2)
			{
				SetForegroundWindow(hwnd);

				//处理有文件正在打开的情况：若是其他文件正在打开，则弹窗提示；若是当前待打开文件，不弹窗。
				//有文件正在打开但文件路径为空时，认为非打开状态，由后续处理
				QString strFilePathOpening;
				if (g_pGMPMainForm->serviceMgr()->isOpening(strFilePathOpening) && !strFilePathOpening.isEmpty())
				{
					if (strFilePathOpening.compare(strFilePath, Qt::CaseInsensitive) != 0)
					{
						//避免重复提醒
						static bool bHasOpeningDlg = false;
						if (!bHasOpeningDlg)
						{
							bHasOpeningDlg = true;
							GMPMessageBox::warning(pMainForm, qApp->translate(c_sGMPUniqueManager, c_sOpenFile),
								qApp->translate(c_sGMPUniqueManager, c_sFileIsOpening));
							bHasOpeningDlg = false;
						}
					}
				}
				else
				{
					if (QApplication::modalWindow() != NULL)
					{
						//避免重复提醒
						static bool bHasModalDlg = false;
						if (!bHasModalDlg)
						{
							bHasModalDlg = true;
							GMPMessageBox::information(pMainForm, qApp->translate(c_sGMPUniqueManager, c_strInfo),
								qApp->translate(c_sGMPUniqueManager, c_sOpenWhenModel));
							bHasModalDlg = false;
						}
						return 0;
					}

					if (pMainForm->isMinimized())
					{
						pMainForm->showMaximized();
						QApplication::processEvents();
					}

					if (!strFilePath.isEmpty() && !g_pGMPMainForm->serviceMgr()->isOpened(strFilePath))
					{
						if (GMPBeginnerGuideDataManager::getInstance()->isShowGuide())
							GMPBeginnerGuideDataManager::getInstance()->closeGuideDlg(false);

						// GMPUSER-483，在windows事件中直接调用deleteLater无效
						QTimer::singleShot(0, [pMainForm, strFilePath]()
						{
							g_pGMPMainForm->serviceMgr()->openProject(strFilePath);
						});

					}
				}
			}
		}
	}
	else if (uMsg == WM_NCHITTEST)
	{
		POINT pt = { LOWORD(lParam), HIWORD(lParam) };
		ScreenToClient(hwnd, &pt);
		//先写死的40*40为系统菜单所在区域
		if (pt.x < 40 && pt.y < 40)
			return HTCLIENT;
		else
			return oldWinProc(hwnd, uMsg, wParam, lParam);
	}
	else if (uMsg == WM_NCLBUTTONDBLCLK && (int)wParam == HTSYSMENU)
	{
		return 0;
	}
	else
	{
		return oldWinProc(hwnd, uMsg, wParam, lParam);
	}
	return 0;
}

/**
* 注册进程间通信的Windows消息.
* @date  2018年4月10日
* @author yangpy
*/
unsigned int initWindowMessage()
{
	//注册windows消息
	unsigned int uMsgId = RegisterWindowMessage(GMPAppInfo::gAppInfo()->productCode().toStdWString().c_str());

	//Vista之后新增了UIPI机制，低权限的进程向高权限进程发送某些消息会受到限制
	//大于WM_USER的消息会被阻止，允许通过的消息必须要添加到过滤器才行
	if ((QSysInfo::windowsVersion() & QSysInfo::WV_NT_based) >= QSysInfo::WV_VISTA)
	{
		HMODULE hUser32 = ::LoadLibraryW(L"user32.dll");
		if (hUser32 != NULL)
		{
			typedef BOOL(WINAPI *DllFunc)(UINT, DWORD);
			DllFunc pFunc = (DllFunc)GetProcAddress(hUser32, "ChangeWindowMessageFilter");
			if (pFunc != NULL)
				pFunc(uMsgId, MSGFLT_ADD);
			FreeLibrary(hUser32);
		}
	}

	return uMsgId;
}

/**
* 广播一个消息通知其它实例激活自己.
* @param[in]    const QString &msgData
* @param[in]    WPARAM wParam
* @date  2018年4月10日
* @author yangpy
*/
void broadcastFocusMessage(const QString &msgData, WPARAM wParam)
{
	int nSize = msgData.toStdString().size() + 1;

	// 创建内存映射
	HANDLE hFile = createMapFileData(c_szAppParamShareName, msgData.toStdString().c_str(), nSize);
	long lResult = -1;
	// 向所有进程广播通知消息
	DWORD bsmRecipients = BSM_APPLICATIONS;

	// 发送广播消息
	while (lResult == -1)
		lResult = BroadcastSystemMessage(BSF_IGNORECURRENTTASK | BSF_NOTIMEOUTIFNOTHUNG | BSF_NOHANG | BSF_POSTMESSAGE,
			&bsmRecipients, g_uMsgId, wParam, nSize);
};

/**
* 是否是唯一实例.
* @param[in]    int argc
* @param[out]   QSharedMemory& memory
* @return       bool
* @date  2018年4月10日
* @author yangpy
*/
bool isUniqueApp()
{
	bool bResult = true;
	QStringList args = getCommandLineArgv();

	QSystemSemaphore sysSemaphore(GMPAppInfo::gAppInfo()->productCode(), 1, QSystemSemaphore::Open);
	sysSemaphore.acquire();
	s_InstanceMem.setKey(GMPAppInfo::gAppInfo()->productName() + GMPAppInfo::gAppInfo()->productCode());

	QString strFilePath = getProjectFilePath(args);
	//存在全局对象，则退出
	bool isSucess = s_InstanceMem.create(1);
	if (!isSucess && strFilePath.isEmpty())
	{
		QString strData;
		broadcastFocusMessage(strData, wp_ACTIVATE);
		bResult = false;
	}
	//双击工程文件A且当前有活动软件，那么关闭当前工程，打开A
	else if (!s_InstanceMem.create(1) && !strFilePath.isEmpty())
	{
		QString strData;
		strData.append(args.at(0));
		strData.append(";");
		strData.append(strFilePath);
		broadcastFocusMessage(strData, wp_OPENFILE);
		bResult = false;
	}

	sysSemaphore.release();
	return bResult;
}

/*!
*@brief  文件是否已经打开过，激活打开进程
*@author zhangzm 2016年8月31日
*@param[in]   int argc
*@return       bool
*/
bool isFileOpened()
{
	bool bResult = false;

	QSystemSemaphore sysSemaphore(GMPAppInfo::gAppInfo()->productCode(), 1, QSystemSemaphore::Open);
	sysSemaphore.acquire();

	QStringList args = getCommandLineArgv();
	QString strFilePath = getProjectFilePath(args);
	if (!strFilePath.isEmpty() && fileIsOpened(strFilePath))
	{
		QString strData;
		broadcastFocusMessage(strData, wp_ACTIVATE);
		bResult = true;
	}

	sysSemaphore.release();
	return bResult;
}

/**
* 文件是否正在打开中。不包括启动软件后手动打开文件的情况，此场景由活动软件在处理打开文件的消息时自行处理
* @param[in]     int argc
* @param[in]     char *argv[]
* @return        bool
* @date          2018年4月27日
* @author        yangpy
*/
bool isFileOpening()
{
	bool bResult = false;

	QSystemSemaphore sysSemaphore(GMPAppInfo::gAppInfo()->productCode(), 1, QSystemSemaphore::Open);
	sysSemaphore.acquire();

	QStringList strParams = getCommandLineArgv();
	if (strParams.count() > 1)
	{
		QString strFilePath = getProjectFilePath(strParams);
		if (fileExists(strFilePath))
		{
			if (GMPGlobalTags::queryTag(strFilePath))
			{
				QString strData;
				broadcastFocusMessage(strData, wp_ACTIVATE);
				bResult = true;
			}
			else
			{
				GMPGlobalTags::setTag(strFilePath);
			}
		}
	}

	sysSemaphore.release();
	return bResult;
}

/**
* 给特定用户组付不同的访问权限
* @param[in]    SECURITY_ATTRIBUTES* pSA 用户权限结构
* @return       bool
* @date         2018年12月29日
* @author       jiawc
*/
BOOL createDACL(SECURITY_ATTRIBUTES* pSA)
{
	// Define the SDDL for the DACL. This example sets 
	// the following access:
	//     Built-in guests are denied all access.
	//     Anonymous logon is denied all access.
	//     Authenticated users are allowed full control access.
	//     Administrators are allowed full control.
	// Modify these values as needed to generate the proper
	// DACL for your application. 
	TCHAR * szSD = TEXT("D:")       // Discretionary ACL
		TEXT("(D;OICI;GA;;;BG)")    // Deny access to built-in guests
		TEXT("(D;OICI;GA;;;AN)")    // Deny access to anonymous logon
		TEXT("(A;OICI;GA;;;AU)")	// Allow full control to authenticated users
		TEXT("(A;OICI;GA;;;BA)");   // Allow full control to administrators

	if (NULL == pSA)
		return FALSE;

	return ConvertStringSecurityDescriptorToSecurityDescriptor(
		szSD,
		SDDL_REVISION_1,
		&(pSA->lpSecurityDescriptor),
		NULL);
}

bool checkMultiUserUnique()
{
	//判断程序之间的互斥
	QString metxStr = "Global\\" + GMPAppInfo::gAppInfo()->productCode();
	SECURITY_ATTRIBUTES sa;
	sa.nLength = sizeof(SECURITY_ATTRIBUTES);
	sa.bInheritHandle = FALSE;
	createDACL(&sa);
	HANDLE hGaeaMutex = ::CreateMutex(&sa, TRUE, metxStr.toStdWString().c_str()); //等进程结束自动释放
	if (GetLastError() == ERROR_ALREADY_EXISTS)
	{
		GMPMessageBox::warning(nullptr, qApp->translate(c_sGMPUniqueManager, c_smainInfo), qApp->translate(c_sGMPUniqueManager, c_sMultipleProcessHit));
		return false;
	};
	return true;
}

/*!
*@brief  是否使用已经启动的程序
*@author zhangzm 2016年8月31日
*@param[in]   int argc
*@param[in]   QSharedMemory& memory
*@return       bool
*/
bool TasGAEAUniqueManager::checkUnique()
{
	g_uMsgId = initWindowMessage();

	//***判断文件是否正在打开，不允许重复打开***
	if (isFileOpening())
		return true;

	if ((GMPAppInfo::gAppInfo()->multiProcess() && isFileOpened())
		|| (!GMPAppInfo::gAppInfo()->multiProcess() && !isUniqueApp()))
	{
		return true;
	}

	if (!checkMultiUserUnique())
		return true;

	return false;
}

/**
* 注册 Mainform.
* @param[in]    GAEAMainForm* pMainForm
* @date  2018年4月17日
* @author yangpy
*/
void TasGAEAUniqueManager::registerMainForm(WId id)
{
	HWND hwnd = reinterpret_cast<HWND>(id);
	oldWinProc = reinterpret_cast<GMPWinProc>(::SetWindowLongPtr(hwnd, GWLP_WNDPROC, reinterpret_cast<__int64>(&newGMPWinProc)));
}

