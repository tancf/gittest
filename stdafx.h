// stdafx.h : 标准系统包含文件的包含文件，
// 或是经常使用但不常更改的
// 特定于项目的包含文件
//
#pragma once
// Windows 头文件:
#include <windows.h>
#include <tuple>
#include <memory>
// std库
#include <set>
#include <vector>
#include <map>
#include <hash_map>
#include <hash_set>
#include <functional>
#include <algorithm>
#include <string.h>
#include <strstream>
#include <assert.h>
#include <iostream>
#include <memory>
//数据库
#include <GGDB/ggdbcommand.h>

#include "ViewManager/GViewer.h"
//QT
#include <QObject>
#include <QString>
#include <QVariant>
#include <QSharedPointer>
#include <QDebug>

#include <QList>
#include <QMap>
#include <QHash>
#include <QVector>
#include <QStringList>
#include <QSet>

#include <qmath.h>
#include <QRect>
#include <QPoint>
#include <QtXml/qdom.h>
#include <QStyledItemDelegate>
#include <QStandardItemModel>
#include <QThread>
#include <QColor>

#include <QApplication>
#include <QKeyEvent>
#include <qevent.h>
#include <QWidget>
#include <QPushButton>
#include <QLineEdit>
#include <QTextEdit>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QGridLayout>
#include <QTabWidget>
#include <QComboBox>
#include <QTimer>
#include <QImageIOHandler>
#include <QImageIOHandler>
#include <QPixmap>
#include <QFile>
#include <QCursor>
#include <QDialog>
#include <QSplitter>
#include <QToolBar>
#include <QGroupBox>
#include <QMenu>
#include <QRadioButton>
#include <QStandardItem>
#include <QTreeWidget>
#include <QAction>
#include <QFrame>
#include <QPushButton>
#include <QMainWindow>
#include <QFocusEvent>
#include <QSignalMapper>
#include <QWidgetAction>
