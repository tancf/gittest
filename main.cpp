#include "stdafx.h"
#include <QWindow>
#include <QResource>
#include <QApplication>
#include <QMainWindow>
#include <qfontdatabase.h>
#include "log.h"
#include "GSC/ANTI/ss_lm_runtime.h"
#include "ViewCore/GGPUSwitch.h" 
#include "GUC/GUC.h"
#include "GUC/GUCUnitConvertor.h"
#include "GLDFileUtils.h"
#include "Common/scopeguard.h"
#include "GMCommon/GMPFileUtils.h"
#include "GMCommon/GMPProcessUtilsEx.h"
#include "GMCommon/GMPTranslations.h"
#include "GMPControls/GMPMessageBox.h"
#include "GMPControls/GMPSplashScreen.h"
#include "GMModel/GMPException.h"
#include "GMPProject/GMPGGDBManager.h"
#include "GMPProject/GMPSystemOptions.h"
#include "GMPProject/GMPAppInfo.h"
#include "GMPCore/GMPGlobalTags.h"
#include "GMPCore/GMPShortcutKeyMgr.h"
#include "GMPCore/GMPBroadcastService.h"
#include "GMPCore/IGMPLicence.h"
#include "GMPUbc/GMPUbc.h"
#include "GMCommon/GMPSharedMemUtils.h"
#include "GMPWidgets/GMPMainForm.h"
#include "GMPCore/GMPApplication.h"
#include "GMPCore/GMPSysCheckUtils.h"
#include "GMPCore/GMPUIPluginService.h"
#include "GMPWidgets/GMPStudyModeUserInfoDlg.h"
#include "GMPCore/GMPUniqueManager.h"
#include "GMPCloudLogin/GMPAccountMgr.h"
#include "GMPCloud/GMPCloud.h"
#include "GMPCloudLogin/GMPLoginHelper.h"
#include "TasGAEAUIPluginService.h"
#include "TasGAEAUniqueManager.h"

#ifdef USETCMALLOC
#pragma comment(lib,"libtcmalloc_minimal.lib")
#pragma comment(linker,"/INCLUDE:__tcmalloc")
#endif

static const char *c_smain = "main";
static const char *c_sTryReinstall = QT_TRANSLATE_NOOP("main", "尝试重新安装程序以解决此问题。");
static const char *c_smainOpen = QT_TRANSLATE_NOOP("main", "打开");
static const char *c_sStudyModeTitle = QT_TRANSLATE_NOOP("main", "（学习版模式）");

/** 双显卡自动切换. */
GGP_FORCE_USE_DISCRETE_GPU;

// DEFINE_RESOURCESTRING(c_sProjectCode, "GMP");
// DEFINE_RESOURCESTRING(c_sProjectName, "SLPT_GMP");
// DEFINE_RESOURCESTRING(c_sProjectVersion, "3.3");
// DEFINE_RESOURCESTRING(c_sProjectExt, "GMP");
// DEFINE_RESOURCESTRING(c_sTemplateEntProjExt, "GFT");
DEFINE_RESOURCESTRING(c_sUnitConfig, "UnitConfig.xml");
//DEFINE_RESOURCESTRING(c_sCalRule, "calRule");

const short c_snProductId = 438;

SS_UINT32 SSAPI app_ss_msg(SS_UINT32 message, void* wparam, void* lparam);
bool initScreenSplash(GMPSplashScreen *pSplash);
bool initGGDB(TasGAEAUIPluginService* pUIPluginService);
bool initStartUpPage(void);
void atAppExit(void);
bool licenceLogin(void);
QString parseProjectFile(int argc, char *argv[]);

int main(int argc, char *argv[])
{
	VMProtectBegin("gaea_main");

	// 2019.5.29 fixed bugABC-2085 使用HD4600显卡的机器在Windows7系统下Ribbon区域显示异常
	if (QT_VERSION >= QT_VERSION_CHECK(5, 6, 0))
		QCoreApplication::setAttribute(Qt::AA_UseOpenGLES); //或者：AA_UseSoftwareOpenGL 

															//2.19.12.17 fixed bug GMP-12489、GMP-12490，解决日志库进程无法退出
	GMPLog4Helper::initialize();

	SCOPE_EXIT
	{
		atAppExit();
	GMPLog4Helper::deinitialize();
	};

	std::tr1::shared_ptr<TasGAEAUIPluginService> pUIPluginService(new TasGAEAUIPluginService);
	GString prjFile = parseProjectFile(argc, argv);
	if (!prjFile.isEmpty())
	{
		//判断工程是否允许打开
		if (pUIPluginService && !pUIPluginService->canOpen(prjFile))
		{
			return 0;
		}
	}

	//***启动反黑机制***
#ifdef ANTIDEBUG 
	SS_UINT32 sts = SS_OK;
	DWORD threadid = 0;
	ST_INIT_PARAM st_init_param = { 0 };

	//VMProtectBegin("ss_init");

#ifndef ANTIBLACK
	//***初始化反黑引擎 并且连接SS***
	st_init_param.version = 1;
	st_init_param.pfn = &(app_ss_msg);
	st_init_param.timeout = 1000 * 60;
#ifndef _WIN64 
	//***第三方反黑64位库有bug(目前无法修复，建议64位程序暂不加反黑处理)***
	sts = slm_init(&(st_init_param));
#endif
#endif
#endif

	//***程序初始化***
	GMPApplication app(argc, argv);
	app.initialize();

	if (pUIPluginService)
	{
		pUIPluginService->initAppInfo();
	}
	

#ifdef ANTIDEBUG
	//***是否使用已经打开的进程***
	if (TasGAEAUniqueManager::checkUnique())
	{
		Sleep(1000);
		return 0;
	}
#endif

	//***初始化产品数据***
	if (!pUIPluginService->loadPlugin())
		return 0;

	//***启动闪屏***
	std::tr1::shared_ptr<GMPSplashScreen> pSplash(new GMPSplashScreen());
	bool bInitSplash = initScreenSplash(pSplash.get());
	if (!bInitSplash)
		return 0; // 返回值参照其它分支返回

				  //***初始化数据库***
	pSplash.get()->setCurrentTip(stInitGGDB);
	if (!initGGDB(pUIPluginService.get()))
		return 0;

	//***系统检查***
	pSplash.get()->setCurrentTip(stSystemCheck);
	if (!execSystemCheck())
		return 0;

	//***初始化单位管理***
	getGUCFuncMgr();
	GUC::GUCFuncMgr::parseUnitConfigXMLFile(GMPAppInfo::gAppInfo()->appDataPath() + c_sUnitConfig);

	//***初始化云服务***
	pSplash.get()->setCurrentTip(stInitCloudSevice);
	initCloudService();

	//***过滤全局的事件以便处理框架快捷键响应***
	pSplash.get()->setCurrentTip(stInitShortCut);
	GMPShortcutKeyMgr::instance()->addShortcutObj(&app);

	//***登录加密锁***
	pSplash.get()->setCurrentTip(stLicenceLogin);
	if (!licenceLogin())
		return 0;

	//***启动云服务(需要在加密锁登录成功后才能启动)***
	pSplash.get()->setCurrentTip(stStartCloudService);
	startCloudService();

	//***初始化开始界面**
	if (!initStartUpPage())
		return 0;

	//***创建主窗体***
	pSplash->setCurrentTip(stMainForm);

	GMPMainForm oMainForm(pUIPluginService.get());
	oMainForm.initialize();

	// 替换消息循环：支持双击打开工程文件
    TasGAEAUniqueManager::registerMainForm(oMainForm.winId());

	// 如果闪屏可见先关闭
	if (pSplash->isVisible())
		pSplash->close();

	oMainForm.show();

	//用户登录设置初始化广播
	GMPBroadcastEventInitUserLoginSetting oInitLoginSettingEvent;
	GMPBroadcastService::sendBroadcast(&oInitLoginSettingEvent);

	// welcome广播
	GMPBroadcastEventWelcome oWelcomeEvent(&oMainForm);
	GMPBroadcastService::sendBroadcast(&oWelcomeEvent);

	//***执行***
	int nRet = app.exec();

	GMPAccountMgr::getLoginIntf()->authLogout();

	//***停止云服务***
	stopCloudService();

	//***删除插件***
	pUIPluginService->unloadPlugin();

	//***退出前刷新一下用户数据***
	GMPGGDBManager::ggdbMgr()->getGGDB(c_sUserInfo, true)->Flush();

	//***加密锁清理***
#ifdef ANTIDEBUG
#ifndef ANTIBLACK
#ifndef _WIN64 
	//liusw 2019.6.6 第三方反黑64位库有bug(目前无法修复，建议64位程序暂不加反黑处理)，所以此处的清理也同样加一下编译控制
	sts = slm_cleanup();
#endif
#endif
#endif
	return nRet;
	VMProtectEnd();
}

/**
* 初始化开始界面.
* @param[in]    void
* @return       bool 初始化是否成功
* @date         2018年4月17日
* @author       yangpy
*/
bool initStartUpPage(void)
{
	GMPBroadcastEventBeforeStartup oBeforeStartupEvent;
	GMPBroadcastService::sendBroadcast(&oBeforeStartupEvent);

	if (oBeforeStartupEvent.reject())
		return false;

	return true;
}

/**
* 初始化反黑.
* @param[in]    SS_UINT32 message
* @param[in]    void* wparam
* @param[in]    void* lparam
* @return       SS_UINT32 SSAPI
* @date         2018年4月10日
* @author       yangpy
*/
SS_UINT32 SSAPI app_ss_msg(SS_UINT32 message, void* wparam, void* lparam)
{
	VMProtectBegin("app_ss_msg");

	SS_UINT32 ret = SS_OK;
	switch (message)
	{
	case SS_ANTI_WARNING:
	{
		switch ((UINT32)(wparam))
		{
		case SS_ANTI_PATCH_INJECT:
		case SS_ANTI_ATTACH_FOUND:
		case SS_ANTI_DEBUGGER_FOUND:
			ret = 1;
			break;
		}
	}
	break;

	case SS_ANTI_EXCEPTION:
		ret = 1;
		break;

	default:
		break;
	}

	VMProtectEnd();
	return ret;
}

/**
* 退出处理.
* @date     2018年4月10日
* @author   yangpy
*/
void atAppExit()
{
	// 登出加密锁
	IGMPLicence *pLicence = GMPSystemOptions::getInstance()->getSysOption(c_wStrLicenceSymble).value<IGMPLicence*>();
	pLicence->logout();
	// 删除快捷管理
	GMPShortcutKeyMgr::freeInstance();
	// 关闭数据库
	GMPGGDBManager::ggdbMgr()->closeAll();
	// 释放COM运行库
	CoUninitialize();
	// 卸载翻译
	GMPTranslations::getInstance()->exitInstance();
	GMPGlobalTags::clearTags();
}

/**
* 加密锁登录并设置启动模式.
* @return       bool 登录是否成功
* @date         2018年4月10日
* @author       yangpy
*/
bool licenceLogin(void)
{
#ifdef ANTIDEBUG
	IGMPLicence *pLicence = GMPSystemOptions::getInstance()->getSysOption(c_wStrLicenceSymble).value<IGMPLicence*>();
#ifndef TRIAL_MODE
	if (!pLicence->login().toBool())
	{

		//yangln 2018年11月23日fixed bug GMP-8389
		//云授权_关闭登录窗体弹出启动学习版提示
		if (!GMPAccountMgr::getLoginIntf()->authIsLogin())
			return false;
		//end fixed bug GMP-8389

		//***学习版模式***
		if (!GMPAppInfo::gAppInfo()->studyMode())
		{
			GlodonMessageBox::warning(NULL, qApp->translate(c_smain, c_smainOpen), pLicence->getPromptInfo(lpiHasNodog).toString());
			return false;
		}

		//通知产品弹出自定义的学习版弹窗
		GMPBroadcastEventStudyModeConform oStudyModeConformEvent;
		oStudyModeConformEvent.setTipInfo(pLicence->getPromptInfo(lpiStudyMode).toString());
		GMPBroadcastService::sendBroadcast(&oStudyModeConformEvent);
		if (oStudyModeConformEvent.reject())
			return false;

		pLicence->setStartUpMode(lmStudyMode);
		//学习版登录
		if (GMPUbc::bUbcEnable())
			GMPUbc::instance()->setExpressEdition(true);
		//if (GMPAppInfo::gAppInfo()->useGCCSTrialServer())
		//{
		//    //GMPAccountMgr::getLoginIntf()->setStudyMode(true);
		//}

		//上传用户信息
		if (!GMPUserUniqueInfo::isCurrentUserInfoCompleted())
		{
			GMPStudyModeUserInfoDlg aUserInfoDlg;
			if (aUserInfoDlg.exec() != QDialog::Accepted ||
				!GMPUserUniqueInfo::updateCurrentUserInfo(aUserInfoDlg.userInfo()))
			{
				//此处应登出，否则学习版将无法在其他地方登录
				GMPAccountMgr::getLoginIntf()->authLogout();
				return false;
			}
			QString strCheckCode = aUserInfoDlg.getCheckCode();
			if (!strCheckCode.isEmpty())
			{
				//用户绑定或修改了手机号码
				QString strMobile = aUserInfoDlg.userInfo().Mobile;
				if (!GMPUserUniqueInfo::bindUserPhone(strCheckCode, strMobile))
					return false;
			}
			//上传老学习版信息
			if (GMPUbc::bUbcEnable())
				GMPUbc::instance()->uploadUserInfo(aUserInfoDlg.userInfo());
		}

		//设置学习版标题
		GString sProductTitle =
			GMPAppInfo::gAppInfo()->productTitle() + qApp->translate(c_smain, c_sStudyModeTitle);
		GMPAppInfo::gAppInfo()->setProductTitle(sProductTitle);

	}
	else
		//***正式版模式***
	{
		std::string strSTDDeviceID = pLicence->getDeviceNum();
		QString strDeviceID = QString(QString::fromLocal8Bit(strSTDDeviceID.c_str()));
		if (GMPUbc::bUbcEnable())

			GMPUbc::instance()->setUbcGD(strDeviceID);

		QString strUserGid = pLicence->getGID().toString();
		if (strUserGid != "" && GMPUbc::instance()->getGlodonCloudUserID().isEmpty())
		{
			GMPUbc::instance()->setGlodonCloudUserID(strUserGid);
		}

		pLicence->setStartUpMode(lmNormalMode);
	}
#else
	//***试用版模式***
	pLicence->setStartUpMode(lmTrialMode);
#endif
#else
	//***DEBUG 模式(需要初始化一下实例)***
	//GAEALicence::instance();
#endif
	return true;
}

/**
* 初始化闪屏.
* @param[in]    void
* @return       bool 国际化的需求，如果启动图的广播产品返回了失败标志，则启动图不会显示，后续程序会直接退出
* @date         2018年4月11日
* @author       yangpy
*/
bool initScreenSplash(GMPSplashScreen *pSplash)
{
	//初始化默认参数
	pSplash->setTitle(GMPAppInfo::gAppInfo()->productTitle());
	pSplash->setPixmap(QPixmap(":/images/splash.png"));

	//产品的定制化需求在广播汇中实现
	GMPBroadcastEventSplash oSplashEvent(pSplash);
	GMPBroadcastService::sendBroadcast(&oSplashEvent);
	if (!oSplashEvent.reject())
	{
		//启动闪屏
		pSplash->show();
	}

	return !oSplashEvent.reject();
}

/**
* 初始化数据库.
* @param[in]    GAEAUIPluginService* pUIPluginService
* @return       bool
* @date         2018年4月13日
* @author       yangpy
*/
bool initGGDB(TasGAEAUIPluginService* pUIPluginService)
{
	try
	{
		pUIPluginService->initializeGGDB();
	}
	catch (const GMPCustomException &e)
	{
		if (e.type() == ceGGDBFileNotExist)
		{
			gmp::confirmDlg(e.message() + "\r\n" + qApp->translate(c_smain, c_sTryReinstall), GlodonMessageBox::Critical, QApplication::activeWindow());
			return false;
		}
		return false;
	}
	catch (...)
	{
		return false;
	}
	return true;
}

//从参数列表中解析工程文件名
QString parseProjectFile(int argc, char *argv[])
{
	if (argc > 1)
	{
		QStringList strParams;
		strParams = getCommandLineArgv();
		QString strFilePath = getProjectFilePath(strParams);
		if (fileExists(strFilePath))
		{
			return strFilePath;
		}
	}

	return "";
}