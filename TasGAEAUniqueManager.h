#pragma once
/**
* 启动进程管理方法.
* @file     TasGAEAUniqueManager.h
* @author   yangpy
* @date     2018年4月16日
* @version 3.0
*/
#ifndef GAEA_UNIQUE_MANAGER_H__
#define GAEA_UNIQUE_MANAGER_H__

#include "GMPWidgets/GMPMainForm.h"

class TasGAEAUniqueManager
{
public:
	static bool checkUnique();
	static void registerMainForm(WId id);
};

#endif