#include "TasGAEAUIPluginService.h"
#include <QSettings>
#include <QStringList>
#include <QLibrary>
#include "GLDString.h"
#include <memory>
#include "GMCommon\GMPFileUtils.h"
#include "GLDFileUtils.h"
#include "GLDStrUtils.h"


#ifdef WIN32
typedef bool *(__stdcall CanOpen)(QString *prjFile);
#else
typedef bool *(__attribute__((stdcall)) CanOpen)(QString *prjFile);
#endif

bool TasGAEAUIPluginService::canOpen(const QString &prjFile)
{
	//空文件允许打开，即直接打开软件
	if (prjFile.isEmpty())
	{
		return true;
	}

	bool bRet = true;
	do
	{
#ifdef NDEBUG
		QSettings setting(exePath() + "Plugins.ini", QSettings::IniFormat);
#else
		QSettings setting(exePath() + "Pluginsd.ini", QSettings::IniFormat);
#endif
		QStringList oGroups = setting.childGroups();
		for (int i = 0; i < oGroups.size(); ++i)
		{
			setting.beginGroup(oGroups.at(i));
			if (setting.value("active", false).toBool() && setting.value("main").toBool())
			{
				QString strLibFileName = exePath() + setting.value("name", "").toString() + ".dll";
				if (!fileExists(strLibFileName))
				{
					setting.endGroup();
					continue;
				}
				QLibrary lib(strLibFileName);
				if (!lib.load())
				{
					setting.endGroup();
					continue;
				}

#if defined(WIN64) || defined(_WIN64) || defined(__WIN64__)
				GString strFuncName = "canOpen";
#else
				GString strFuncName = "_canOpen@4";
#endif

				CanOpen *func = (CanOpen *)lib.resolve(strFuncName.toLatin1().data());
				if (nullptr != func)
				{
					bRet = func((QString *)&prjFile);
					setting.endGroup();
					break;
				}
			}
			setting.endGroup();
		}
	} while (false);

	return bRet;
}

